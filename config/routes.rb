Rails.application.routes.draw do
  root 'collections#index'

  concern :votable do
    resources :votes, only: [:create, :destroy]
  end

  resources :collections do
    resources :collection_items
  end

  resources :movies do
    concerns :votable
  end

  devise_for :users
end
