require 'rails_helper'

RSpec.describe Collection, type: :model do
  it { is_expected.to have_many(:collection_items) }
  it { is_expected.to belong_to(:user) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:user) }
  it { is_expected.to validate_length_of(:description).is_at_most(300) }
end
