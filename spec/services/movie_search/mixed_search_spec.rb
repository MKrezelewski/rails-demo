require 'rails_helper'

describe MovieSearch::MixedSearch do
  let(:client) { double("client") }

  let(:search_params) { { title: "match" } }

  let(:first_movie) { double("first movie", title: "Movie 1", year: 2016, genre: "action", country: "USA") }
  let(:second_movie) { double("second movie", title: "Movie 2", year: 1993, genre: "comedy", country: "UK") }

  let(:matching_entries) { 6 }
  let(:not_matching_entries) { 3 }

  before { mock_dependencies }

  describe "search results" do
    context "when database already has enough matching entries" do
      before { populate_database(matching_entries, not_matching_entries) }

      it "doesn't use API client" do
        expect(MovieDatabase::Client).not_to have_received(:new)

        subject.search(search_params)
      end

      it "returns matching movies from app database" do
        expect(subject.search(search_params).count).to eq matching_entries
      end
    end

    context "when database has not enough matching entries" do
      context "when some API results are already in database" do
        before { FactoryGirl.create(:movie, title: first_movie.title, year: first_movie.year) }

        it "doesn't add them to database again" do
          expect { subject.search(search_params) }.to change { Movie.count }.by(1)
        end
      end

      it "adds matching entries from API" do
        expect { subject.search(search_params) }.to change { Movie.count }.by(2)
      end
    end
  end

  def mock_dependencies
    allow(MovieDatabase::Client).to receive(:new).and_return(client)
    allow(client).to receive(:query).with({title: "match"}).and_return([first_movie, second_movie])
  end

  def populate_database(matching = 1, not_matching = 2)
    matching.times { |i| FactoryGirl.create(:movie, title: "Will match #{i}") }
    not_matching.times { |i| FactoryGirl.create(:movie, title: "Will not #{i}")}
  end
end
