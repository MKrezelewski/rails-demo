FactoryGirl.define do
  factory :collection_item do
    collection nil
    collectible nil
    collectible_type "MyString"
  end
end
