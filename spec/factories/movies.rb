FactoryGirl.define do
  factory :movie do
    title "MyString"
    year 1
    genre "MyString"
    country "MyString"
  end
end
