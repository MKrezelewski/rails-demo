require 'rails_helper'

shared_examples "space with restricted access" do |path|
  before { sign_in }

  it "is accessible to signed users" do
    visit path

    expect(current_path).to eq path
  end

  it "redirects unsigned users to login page" do
    sign_out

    visit path

    expect(current_path).to eq new_user_session_path
  end
end
