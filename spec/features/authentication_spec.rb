require "rails_helper"

RSpec.feature "Authentication", type: :feature do
  let!(:user) { FactoryGirl.create(:user) }

  before { visit '/' }

  scenario "Signing in" do
    expect(page).to have_text("You need to sign in or sign up before continuing.")

    expect(page).to have_text("Log in")

    fill_in "Email", with: "test@example.com"
    fill_in "Password", with: "example123"

    click_button "Log in"

    expect(page).to have_text "Signed in successfully."
  end

  scenario "Failing to sign in" do
    expect(page).to have_text("Log in")

    fill_in "Email", with: "test@example.com"
    fill_in "Password", with: "wrong password"

    click_button "Log in"

    expect(page).to have_text "Invalid email or password."
  end

  scenario "Signing out" do
    fill_in "Email", with: "test@example.com"
    fill_in "Password", with: "example123"

    click_button "Log in"

    expect(page).to have_link "Sign out"
    click_link "Sign out"

    expect(page).to have_text "You need to sign in or sign up before continuing."
  end
end
