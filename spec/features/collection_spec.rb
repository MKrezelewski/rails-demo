require "rails_helper"

RSpec.feature "Collection page", type: :feature do
  it_behaves_like "space with restricted access", '/collections'
end
