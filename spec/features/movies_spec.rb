require "rails_helper"

RSpec.feature "Movies page", type: :feature do
  it_behaves_like "space with restricted access", '/movies'
end
