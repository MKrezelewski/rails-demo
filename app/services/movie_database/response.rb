module MovieDatabase
  class Response
    attr_reader :raw_response

    def initialize(response)
      @raw_response = response
    end

    def success?
      status == 200
    end

    def status
      @status ||= raw_response.code.to_i
    end

    def extract_movies
      results = parse_response
      movies = []

      results.each { |result| movies << MovieDatabase::Movie.new(title: result["Title"], year: result["Year"].to_i) }

      movies
    end

    private

    def parse_response
      JSON.parse(raw_response.body)["Search"]
    end
  end
end
