module MovieDatabase
  class Movie
    ALLOWED_PARAMS = %i(title, year, genre, country)
    attr_reader *ALLOWED_PARAMS

    def initialize(options = {})
      options.slice(*ALLOWED_PARAMS).each do |param, value|
        instance_variable_set "@#{param}", value
      end
    end
  end
end
