module MovieDatabase
  class Request
    attr_reader :criteria, :api_url

    def initialize(api_url, criteria)
      @api_url = api_url
      @criteria = {
        s: criteria[:title],
        y: criteria[:year],
        type: "movie",
        r: "json"
      }
    end

    def valid?
      criteria[:s].present?
    end

    def to_uri
      URI.parse(api_url + '?' + criteria.compact.to_param)
    end
  end
end
