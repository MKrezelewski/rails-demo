require 'net/http'

module MovieDatabase
  class Client
    API_URL = 'http://www.omdbapi.com/'
    attr_reader :api_url


    def initialize(options = {})
      @api_url = options[:api_url] || API_URL
    end

    def query(criteria = {})
      request = Request.new(api_url, criteria)
      if request.valid?
        response = perform_request(request)

        handle_response(response)
      else
        raise InvalidRequestCriteria
      end
    end

    private

    def perform_request(request)
      uri = request.to_uri
      request_object = Net::HTTP::Get.new(uri.to_s)
      response = Net::HTTP.start(uri.host, uri.port) { |http| http.request(request_object) }
    end

    def handle_response(response)
      processed_response = MovieDatabase::Response.new(response)

      if processed_response.success?
        processed_response.extract_movies
      else
        raise InvalidResponse, "Status: #{response.status}"
      end
    end
  end
end
