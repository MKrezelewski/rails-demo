module MovieSearch
  class MixedSearch
    THRESHOLD = 5

    def search(search_params)
      movies_in_database = Movie.where('lower(title) LIKE ?', "%#{search_params[:title].downcase}%")
      if movies_in_database.count < THRESHOLD
        results = client.query(title: search_params[:title])
        populate_database(results)
      end

      #this scope will have newly added movies
      movies_in_database
    end

    private

    def client
      @client ||= MovieDatabase::Client.new
    end

    def populate_database(results)
      ids = []

      results.each do |result|
        unless Movie.find_by(title: result.title, year: result.year)
          movie = Movie.create(title: result.title, year: result.year, genre: result.genre, country: result.country)
          ids << movie.id
        end
      end

      Movie.where(id: ids)
    end
  end
end
