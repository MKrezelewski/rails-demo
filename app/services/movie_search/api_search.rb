module MovieSearch
  class ApiSearch
    def search(search_params)
      results = client.query(title: search_params[:title])
      populate_database(results)
    end

    private

    def client
      @client ||= MovieDatabase::Client.new
    end

    def populate_database(results)
      ids = []

      results.each do |result|
        movie = Movie.create(title: result.title, year: result.year, genre: result.genre, country: result.country)
        ids << movie.id
      end

      Movie.where(id: ids)
    end
  end
end
