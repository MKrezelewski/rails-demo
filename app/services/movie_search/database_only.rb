module MovieSearch
  class DatabaseOnly
    def search(search_params)
      Movie.where('lower(title) LIKE ?', "%#{search_params[:title].downcase}%")
    end
  end
end
