class CollectionItemsController < ApplicationController
  before_action :set_collection
  before_action :set_collection_item, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @collection_items = @collection.collection_items.all
  end

  def show
  end

  def new
    @collection_item = @collection.collection_items.new
  end

  def edit
  end

  def create
    @collection_item = @collection.collection_items.build(collection_item_params)

    respond_to do |format|
      if @collection_item.save
        format.html { redirect_to [@collection, @collection_item], notice: 'Collection item was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @collection_item.update(collection_item_params)
        format.html { redirect_to [@collection, @collection_item], notice: 'Collection item was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @collection_item.destroy
    respond_to do |format|
      format.html { redirect_to collection_items_url, notice: 'Collection item was successfully destroyed.' }
    end
  end

  private

  def set_collection
    @collection = current_user.collections.find(params[:collection_id])
  end

  def set_collection_item
    @collection_item = @collection.collection_items.find(params[:id])
  end

  def collection_item_params
    params.require(:collection_item).permit(:collectible_id, :collectible_type)
  end
end
