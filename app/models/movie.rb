class Movie < ActiveRecord::Base
  include Votable

  validates :title, presence: true

  def self.search(search_params)
    search_strategy = MovieSearch::MixedSearch.new
    search_strategy.search(search_params)
  end
end
