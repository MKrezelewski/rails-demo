class CollectionItem < ActiveRecord::Base
  belongs_to :collection
  belongs_to :collectible, polymorphic: true

  validates :collection, presence: true
  validates :collectible, presence: true

  delegate :user, to: :collection
end
