class Collection < ActiveRecord::Base
  has_many :collection_items
  belongs_to :user

  validates :name, presence: true
  validates :description, length: { maximum: 300 }
  validates :user, presence: true
end
