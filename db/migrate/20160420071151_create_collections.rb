class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.string :name, null: false
      t.text :description, default: ""
      t.references :user, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
