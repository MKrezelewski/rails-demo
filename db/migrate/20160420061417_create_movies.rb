class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :title, null: false
      t.integer :year
      t.string :genre, default: ""
      t.string :country, default: ""

      t.timestamps null: false
    end

    add_index :movies, :title
  end
end
