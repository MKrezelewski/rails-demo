class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :value
      t.references :user, index: true, foreign_key: true, null: false
      t.references :votable, index: true, polymorphic: true, null: false

      t.timestamps null: false
    end
  end
end
