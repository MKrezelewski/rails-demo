class CreateCollectionItems < ActiveRecord::Migration
  def change
    create_table :collection_items do |t|
      t.references :collection, index: true, foreign_key: true, null: false
      t.references :collectible, index: true, polymorphic: true, null: false

      t.timestamps null: false
    end
  end
end
